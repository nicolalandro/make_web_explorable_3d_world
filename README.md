# Simple explorable 3D world
This is a simple Babylon.js web page that charge a 3D world on the web.

The steps:
* create your room 
  * [SweetHome3D](http://www.sweethome3d.com/it/)
  * export as .obj
* Export for babylonjs  
  * import in [Blender](https://www.blender.org/download/)
  * install [babylonjs plugin](https://github.com/BabylonJS/BlenderExporter/blob/master/Blender2Babylon-6.4.zip)
  * export into statics/world.babylon
* run local server to test 
  * from a terminal with [python3](https://www.python.org/) into the folder:
  * Install [flask](https://flask.palletsprojects.com/en/1.1.x/) `python3 -m pip install flask`
  * Run the server at localhost:5000 `python3 main.py`
* to deploy online free
  * sing in on [Altervista](https://it.altervista.org)
  * create a folder
  * put inside all contents of templates and statics
  * open the index.html
