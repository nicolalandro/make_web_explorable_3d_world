from flask import Flask, render_template, send_from_directory
app = Flask(__name__)

@app.route('/')
def home_page():
    return render_template('index.html')


@app.route('/<path:path>')
def statics(path):
    return send_from_directory('statics/', path)



if __name__ == "__main__":
    app.run()